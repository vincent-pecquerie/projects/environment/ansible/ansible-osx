#!/bin/sh
echo "Etape 1 : Installation de brew."
if ! command -v brew &> /dev/null
then
    /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
    echo 'eval "$(/opt/homebrew/bin/brew shellenv)"' >> /Users/vpecquerie/.zprofile
    eval "$(/opt/homebrew/bin/brew shellenv)" 
fi

echo "Etape 2 : Installation d'Ansible."
if ! command -v ansible &> /dev/null
then
    brew install Ansible
fi

echo "Etape 3 : Installation de XCODE (nécessaire pour avoir GIT)."
xcode-select --install

echo "Etape 4: Installation de ROSETTA"
softwareupdate --install-rosetta

echo "Etape 5 : Installation de GIT."
brew install git

echo "Etape 6 : Clonage du dépôt GIT."
git clone https://gitlab.com/vincent-pecquerie/projects/environment/ansible/ansible-osx.git ~/.ansible_sources

echo "Etape 7 : Initialisation d'Ansible."
cd ~/.ansible_sources && sudo ansible-playbook osx.yml -i hosts --vault-password-file=.vault_pass
